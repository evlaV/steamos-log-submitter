# SPDX-License-Identifier: LGPL-2.1-or-later
# vim:ts=4:sw=4:et
#
# Copyright (c) 2023 Valve Software
# Maintainer: Vicki Pfau <vi@endrift.com>
class HelperError(NotImplementedError):
    pass


class LockHeldError(RuntimeError):
    pass


class LockNotHeldError(RuntimeError):
    pass


class RateLimitingError(RuntimeError):
    pass
